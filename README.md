You might have to use gem to install bundler 
`gem install jekyll bundler`
you might have to use sudo 

To install dependencies,
`bundle install`

for build,
`bundle exec jekyll build`

to serve the development site,
`bundle exec jekyll serve`
This option automatically watches for any changes in the website and reloads accordingly

This site is managed by (Octopress)[https://github.com/octopress/octopress#deploy]

We are deploying via an rsync hook to my DO server.

When you have changes you need to make live,
1. Make sure you are on master
2. `octopress deploy`

Huge credits to

Ayesha Rana, for designing the website, all the assets, and keeping me motivated through this.

Harshal Bhatia, who's knowledge of building front end was invaluable during the initial phase, and whose work I later built on top of to get the website to where it is today.


Based on the Skinny Bones theme.
